using Unity.Entities;
using Unity.Mathematics;

namespace DOTSSpriteRenderer.Components {
    public struct SpriteData : IComponentData {
        public float4x4 Value;
    }
}
