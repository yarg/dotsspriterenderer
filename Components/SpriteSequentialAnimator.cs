using DOTSSpriteRenderer.Types;
using Unity.Entities;

namespace DOTSSpriteRenderer.Components {
    public struct SpriteSequentialAnimator : IComponentData {
        public double     StartTime;
        public ushort     Animation;
        public ushort     NextAnimation;
        public Entity     FramesHolder;
        public RepeatMode RepeatMode;
        public byte       FPS;

        //2 bytes gap. change StartTime to float?
    }
}
