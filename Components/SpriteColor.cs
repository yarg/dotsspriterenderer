using Unity.Entities;
using UnityEngine;

namespace DOTSSpriteRenderer.Components {
    [WriteGroup(typeof(SpriteData))]
    public struct SpriteColor : IComponentData {
        public Color Mul;
        public Color Add;

        public static SpriteColor Default => new SpriteColor {Mul = Color.white, Add = Color.clear};
    }
}
