using System;
using Unity.Entities;
using UnityEngine;

namespace DOTSSpriteRenderer.Components {
    [MaximumChunkCapacity(1024)] //TODO: detect how much
    public struct SpriteTexture : ISharedComponentData, IEquatable<SpriteTexture> {
        public Texture Texture;
        public Material Material;

        #region Equality members
        public bool Equals(SpriteTexture other) {
            return (ReferenceEquals(Texture, null) ? ReferenceEquals(other.Texture, null) : Texture.GetNativeTexturePtr() == other.Texture.GetNativeTexturePtr()) &&
                   (ReferenceEquals(Material, null) ? ReferenceEquals(other.Material, null) : Material.Equals(other.Material));
        }

        public override int GetHashCode() {
            var v = 0;
            if (!ReferenceEquals(Texture, null)) {
                v ^= Texture.GetHashCode();
            }

            if (!ReferenceEquals(Material, null)) {
                v ^= Material.GetHashCode();
            }

            return v;
        }
        #endregion
    }
}
