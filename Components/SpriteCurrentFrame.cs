using DOTSSpriteRenderer.BufferElements;
using Unity.Entities;
using Unity.Mathematics;

namespace DOTSSpriteRenderer.Components {
    public struct SpriteCurrentFrame : IComponentData {
        public float4 UV;
        public float2 Size;
        public float2 Offset;

        public static explicit operator SpriteCurrentFrame(SpriteFrame frame) {
            return new SpriteCurrentFrame {
                    Offset = frame.Offset,
                    Size   = frame.Size,
                    UV     = frame.UV,
            };
        }
    }
}
