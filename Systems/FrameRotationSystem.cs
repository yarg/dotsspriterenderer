using DOTSHelpers.Systems;
using DOTSSpriteRenderer.BufferElements;
using DOTSSpriteRenderer.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace DOTSSpriteRenderer.Systems {
    [UpdateInGroup(typeof(SpriteRendererSystemGroup))]
    [UpdateBefore(typeof(SpriteRenderSystem))]
    public class FrameRotationSystem : JobComponentSystemWithHandles {
        private EntityQuery        _animatorsGroup;
        private SpriteRenderSystem _spriteRenderSystem;

        #region Overrides of ComponentSystemBase
        protected override void OnCreate() {
            base.OnCreate();
            _animatorsGroup = GetEntityQuery(ComponentType.ReadOnly<SpriteRotationAnimator>(),
                                             ComponentType.ReadOnly<Rotation2D>(),
                                             ComponentType.ReadWrite<SpriteCurrentFrame>());
            _spriteRenderSystem = World.GetOrCreateSystem<SpriteRenderSystem>();
        }
        #endregion

        #region UpdateSpriteIndexJob
        [BurstCompile]
        private struct UpdateFrameJob : IJobChunk {
            [ReadOnly] public ArchetypeChunkBufferType<SpriteFrame>           FrameBufferType;
            [ReadOnly] public ArchetypeChunkComponentType<Rotation2D>         RotationComponentType;
            public            ArchetypeChunkComponentType<SpriteCurrentFrame> CurrentFrameComponentType;

            private const float Pi2 = 2f * math.PI;

            #region Implementation of IJobChunk
            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                var frameBuffers  = chunk.GetBufferAccessor(FrameBufferType);
                var rotations     = chunk.GetNativeArray(RotationComponentType);
                var currentFrames = chunk.GetNativeArray(CurrentFrameComponentType);

                for (var i = 0; i < chunk.Count; i++) {
                    var frames       = frameBuffers[i];
                    var rotation     = rotations[i];
                    var framesLength = frames.Length;

                    var angle = rotation.Angle;
                    angle = angle < 0 ? Pi2 + angle : angle;

                    var frameIndex = (int)math.round(angle / Pi2 * framesLength) % framesLength;
                    var frame      = frames[frameIndex];

                    currentFrames[i] = (SpriteCurrentFrame)frame;
                }
            }
            #endregion
        }
        #endregion

        #region Overrides of JobComponentSystem
        protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
            inputDeps = new UpdateFrameJob {
                    FrameBufferType           = GetArchetypeChunkBufferType<SpriteFrame>(true),
                    RotationComponentType     = GetArchetypeChunkComponentType<Rotation2D>(true),
                    CurrentFrameComponentType = GetArchetypeChunkComponentType<SpriteCurrentFrame>(),
            }.Schedule(_animatorsGroup, inputDeps);

            _spriteRenderSystem.AddDependency(inputDeps);
            return inputDeps;
        }
        #endregion
    }
}
