using System.Collections.Generic;
using DOTSHelpers.Systems;
using DOTSSpriteRenderer.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.Rendering;
using NDPFR = Unity.Collections.NativeDisableParallelForRestrictionAttribute;

namespace DOTSSpriteRenderer.Systems {
    //TODO: frustum culling
    //TODO: Profiler.EmitFrameMetaData();
    [UpdateInGroup(typeof(SpriteRendererSystemGroup))]
    [UpdateBefore(typeof(SpriteBlitSystem))]
    public class SpriteRenderSystem : JobComponentSystemWithHandles {
        internal readonly List<SpriteTexture> AllTextures = new List<SpriteTexture>();

        public readonly   List<NativeList<float4x4>> TransformBufferArr = new List<NativeList<float4x4>>();
        internal readonly List<NativeList<float4x4>> DataBufferArr      = new List<NativeList<float4x4>>();

        private  EntityQuery _sprites;
        internal int         PassCount;

        #region Overrides of ComponentSystemBase
        protected override void OnCreate() {
            base.OnCreate();

            _sprites = GetEntityQuery(new EntityQueryDesc {
                    All = new[] {
                            ComponentType.ReadOnly<SpriteCurrentFrame>(),
                            ComponentType.ReadOnly<SpriteTexture>(),
                    },
            });
        }

        protected override void OnDestroy() {
            base.OnDestroy();
            AllTextures.Clear();

            foreach (var nativeList in TransformBufferArr) {
                nativeList.Dispose();
            }

            TransformBufferArr.Clear();

            foreach (var nativeList in DataBufferArr) {
                nativeList.Dispose();
            }
        }
        #endregion

        #region CollectSpriteDataJob
        [BurstCompile]
        private struct CollectSpriteDataJob : IJobChunk {
            [NDPFR]    public NativeArray<float4x4>                               Transforms;
            [NDPFR]    public NativeArray<float4x4>                               Data;
            [ReadOnly] public ArchetypeChunkComponentType<Translation>            TranslationComponentType;
            [ReadOnly] public ArchetypeChunkComponentType<Rotation2D>             RotationComponentType;
            [ReadOnly] public ArchetypeChunkComponentType<Scale>                  ScaleComponentType;
            [ReadOnly] public ArchetypeChunkComponentType<NonUniformScale>        NonUniformScaleComponentType;
            [ReadOnly] public ArchetypeChunkComponentType<SpriteRotationAnimator> FrameRotationComponentType;
            [ReadOnly] public ArchetypeChunkComponentType<SpriteCurrentFrame>     CurrentFrameComponentType;
            [ReadOnly] public ArchetypeChunkComponentType<SpriteData>             DataComponentType;

            #region Implementation of IJobChunk
            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                NativeArray<Translation> translations = default;
                if (chunk.Has(TranslationComponentType)) {
                    translations = chunk.GetNativeArray(TranslationComponentType);
                }

                NativeArray<Scale> scales = default;
                if (chunk.Has(ScaleComponentType)) {
                    scales = chunk.GetNativeArray(ScaleComponentType);
                }

                NativeArray<NonUniformScale> nonUniformScales = default;
                if (chunk.Has(NonUniformScaleComponentType)) {
                    nonUniformScales = chunk.GetNativeArray(NonUniformScaleComponentType);
                }

                NativeArray<Rotation2D> rotations = default;
                if (chunk.Has(RotationComponentType) && !chunk.Has(FrameRotationComponentType)) {
                    rotations = chunk.GetNativeArray(RotationComponentType);
                }

                NativeArray<SpriteData> spriteData = default;
                if (chunk.Has(DataComponentType)) {
                    spriteData = chunk.GetNativeArray(DataComponentType);
                }

                var currentFrames = chunk.GetNativeArray(CurrentFrameComponentType);

                for (var i = 0; i < chunk.Count; i++) {
                    var frame = currentFrames[i];
                    var uv    = frame.UV;
                    var sz    = frame.Size;
                    var of    = frame.Offset;
                    var ts    = float3.zero;
                    var sc    = new float2(1, 1);
                    var zero  = 0;
                    var rota  = 0f;

                    if (translations.IsCreated) {
                        ts = translations[i].Value;
                    }

                    if (rotations.IsCreated) {
                        rota = rotations[i].Angle;
                    }

                    if (nonUniformScales.IsCreated) {
                        var nus = nonUniformScales[i];
                        sc.x = nus.Value.x;
                        sc.y = nus.Value.y;
                    }
                    else if (scales.IsCreated) {
                        sc.x = sc.y = scales[i].Value;
                    }

                    Transforms[firstEntityIndex + i] = new float4x4(ts.x, ts.y, ts.z, rota,
                                                                    uv.x, uv.y, uv.z, uv.w,
                                                                    sz.x, sz.y, of.x, of.y,
                                                                    sc.x, sc.y, zero, zero);

                    var data = float4x4.zero;

                    if (spriteData.IsCreated) {
                        data = spriteData[i].Value;
                    }

                    Data[firstEntityIndex + i] = data;
                }
            }
            #endregion
        }
        #endregion

        #region Overrides of JobComponentSystemWithHandles
        protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
            AllTextures.Clear();
            EntityManager.GetAllUniqueSharedComponentData(AllTextures);

            PassCount = AllTextures.Count;

            var outputDeps = new JobHandle();

            for (var i = 0; i < PassCount; i++) {
                var spriteTexture = AllTextures[i];

                _sprites.SetSharedComponentFilter(spriteTexture);
                var entityCount = _sprites.CalculateEntityCount();

                if (TransformBufferArr.Count <= i) {
                    TransformBufferArr.Add(new NativeList<float4x4>(entityCount, Allocator.Persistent));
                    DataBufferArr.Add(new NativeList<float4x4>(entityCount,      Allocator.Persistent));
                }

                var transformBuf = TransformBufferArr[i];
                var dataBuf      = DataBufferArr[i];

                if (entityCount == 0) {
                    transformBuf.Resize(0, NativeArrayOptions.UninitializedMemory);
                    dataBuf.Resize(0, NativeArrayOptions.UninitializedMemory);
                    continue;
                }

                transformBuf.Resize(entityCount, NativeArrayOptions.UninitializedMemory);
                dataBuf.Resize(entityCount, NativeArrayOptions.UninitializedMemory);

                outputDeps = JobHandle.CombineDependencies(outputDeps, new CollectSpriteDataJob {
                        Transforms                   = transformBuf.AsArray(),
                        Data                         = dataBuf.AsArray(),
                        TranslationComponentType     = GetArchetypeChunkComponentType<Translation>(true),
                        ScaleComponentType           = GetArchetypeChunkComponentType<Scale>(true),
                        RotationComponentType        = GetArchetypeChunkComponentType<Rotation2D>(true),
                        FrameRotationComponentType   = GetArchetypeChunkComponentType<SpriteRotationAnimator>(true),
                        NonUniformScaleComponentType = GetArchetypeChunkComponentType<NonUniformScale>(true),
                        DataComponentType            = GetArchetypeChunkComponentType<SpriteData>(true),
                        CurrentFrameComponentType    = GetArchetypeChunkComponentType<SpriteCurrentFrame>(true),
                }.Schedule(_sprites, inputDeps));
            }

            return outputDeps;
        }
        #endregion
    }
}
