using DOTSHelpers.Systems;
using DOTSSpriteRenderer.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace DOTSSpriteRenderer.Systems {
    [UpdateInGroup(typeof(SpriteRendererSystemGroup))]
    [UpdateBefore(typeof(FrameRotationSystem))]
    public class SpriteRotationSystem : JobComponentSystemWithHandles {
        private EntityQuery _rotatedSpriteGroup;

        #region Overrides of ComponentSystemBase
        protected override void OnCreate() {
            base.OnCreate();
            _rotatedSpriteGroup = GetEntityQuery(new EntityQueryDesc {
                    All = new[] {
                            ComponentType.ReadOnly<Rotation>(),
                            ComponentType.ReadWrite<Rotation2D>(),
                    }
            });
            _rotatedSpriteGroup.AddChangedVersionFilter(typeof(Rotation));
        }
        #endregion

        #region ConvertRotationJob
        [BurstCompile]
        private struct ConvertRotationJob : IJobChunk {
            [ReadOnly] public ArchetypeChunkComponentType<Rotation>   RotationComponentType;
            [ReadOnly] public ArchetypeChunkComponentType<Rotation2D> Rotation2DComponentType;

            #region Implementation of IJobChunk
            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                var rotations   = chunk.GetNativeArray(RotationComponentType);
                var rotations2d = chunk.GetNativeArray(Rotation2DComponentType);

                for (var i = 0; i < chunk.Count; i++) {
                    var v = math.rotate(rotations[i].Value, new float3(1, 0, 0));
                    rotations2d[i] = new Rotation2D {Angle = math.atan2(v.y, v.x)};
                }
            }
            #endregion
        }
        #endregion

        #region Overrides of JobComponentSystemWithHandles
        protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
            return new ConvertRotationJob {
                    RotationComponentType   = GetArchetypeChunkComponentType<Rotation>(),
                    Rotation2DComponentType = GetArchetypeChunkComponentType<Rotation2D>(),
            }.Schedule(_rotatedSpriteGroup, inputDeps);
        }
        #endregion
    }
}
