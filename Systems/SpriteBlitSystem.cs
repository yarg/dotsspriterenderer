using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;

namespace DOTSSpriteRenderer.Systems {
    [UpdateInGroup(typeof(SpriteRendererSystemGroup))]
    public class SpriteBlitSystem : ComponentSystem {
        private readonly Bounds              _bounds           = new Bounds(Vector3.zero, new Vector3(float.MaxValue, float.MaxValue, float.MaxValue));
        private readonly uint[]              _argsBufferData   = {6, 0, 0, 0, 0};
        private readonly List<Material>      _materials        = new List<Material>();
        private readonly List<ComputeBuffer> _argsBuffers      = new List<ComputeBuffer>();
        private readonly List<ComputeBuffer> _transformBuffers = new List<ComputeBuffer>();
        private readonly List<ComputeBuffer> _dataBuffers      = new List<ComputeBuffer>();

        private SpriteRenderSystem _spriteRenderSystem;
        private Mesh               _mesh;
        private int                _transformBufferPropertyName;

        private int _dataBufferPropertyName;

        public Camera Camera;

        #region Overrides of ComponentSystemBase
        protected override void OnCreate() {
            base.OnCreate();
            _spriteRenderSystem          = World.GetOrCreateSystem<SpriteRenderSystem>();
            _mesh                        = CreateMesh();
            _transformBufferPropertyName = Shader.PropertyToID("transformBuffer");
            _dataBufferPropertyName      = Shader.PropertyToID("dataBuffer");
        }
        #endregion

        protected override void OnDestroy() {
            base.OnDestroy();

            foreach (var computeBuffer in _argsBuffers) {
                computeBuffer?.Dispose();
            }

            _argsBuffers.Clear();

            foreach (var computeBuffer in _transformBuffers) {
                computeBuffer?.Dispose();
            }

            _transformBuffers.Clear();

            foreach (var computeBuffer in _dataBuffers) {
                computeBuffer?.Dispose();
            }

            _dataBuffers.Clear();
        }

        #region Overrides of ComponentSystem
        protected override void OnUpdate() {
            if (!Camera) {
                Camera = Camera.main;
            }

            var srs = _spriteRenderSystem;
            srs.FinalJobHandle.Complete();

            for (var i = 0; i < srs.PassCount; i++) {
                if (srs.TransformBufferArr.Count <= i) {
                    break;
                }

                if (_argsBuffers.Count <= i) {
                    _argsBuffers.Add(new ComputeBuffer(5, sizeof(uint)));
                }

                var instanceCount = srs.TransformBufferArr[i].Length;

                if (_transformBuffers.Count <= i) {
                    _transformBuffers.Add(null);
                }

                if (_dataBuffers.Count <= i) {
                    _dataBuffers.Add(null);
                }

                if (_materials.Count <= i) {
                    _materials.Add(null);
                }

                if (instanceCount == 0) {
                    continue;
                }

                _argsBufferData[1] = (uint)instanceCount;
                _argsBuffers[i].SetData(_argsBufferData);

                if ((_transformBuffers[i] == null) || (_transformBuffers[i].count < instanceCount)) {
                    _transformBuffers[i]?.Dispose();
                    _transformBuffers[i] = new ComputeBuffer(instanceCount, sizeof(float) * 16);
                }

                if ((_dataBuffers[i] == null) || (_dataBuffers[i].count < instanceCount)) {
                    _dataBuffers[i]?.Dispose();
                    _dataBuffers[i] = new ComputeBuffer(instanceCount, sizeof(float) * 16);
                }

                _transformBuffers[i].SetData<float4x4>(srs.TransformBufferArr[i]);
                _dataBuffers[i].SetData<float4x4>(srs.DataBufferArr[i]);

                var spriteTexture = srs.AllTextures[i];

                if (ReferenceEquals(_materials[i], null)) {
                    _materials[i] = new Material(spriteTexture.Material);
                }

                var material    = _materials[i];
                var refMaterial = spriteTexture.Material;

                material.CopyPropertiesFromMaterial(refMaterial);
                material.shader      = refMaterial.shader;
                material.mainTexture = spriteTexture.Texture;

                material.SetBuffer(_transformBufferPropertyName, _transformBuffers[i]);
                material.SetBuffer(_dataBufferPropertyName,      _dataBuffers[i]);

                Graphics.DrawMeshInstancedIndirect(_mesh, 0, material, _bounds, _argsBuffers[i], 0, null, ShadowCastingMode.Off, false, 0, Camera, LightProbeUsage.Off, null);
            }
        }
        #endregion

        private static Mesh CreateMesh() {
            var mesh     = new Mesh();
            var vertices = new Vector3[4];
            vertices[0]   = new Vector3(-.5f, -.5f, 0);
            vertices[1]   = new Vector3(.5f,  -.5f, 0);
            vertices[2]   = new Vector3(-.5f, .5f,  0);
            vertices[3]   = new Vector3(.5f,  .5f,  0);
            mesh.vertices = vertices;

            var tri = new int[6];
            tri[0]         = 0;
            tri[1]         = 2;
            tri[2]         = 1;
            tri[3]         = 2;
            tri[4]         = 3;
            tri[5]         = 1;
            mesh.triangles = tri;

            var uv = new Vector2[4];
            uv[0]   = new Vector2(0, 0);
            uv[1]   = new Vector2(1, 0);
            uv[2]   = new Vector2(0, 1);
            uv[3]   = new Vector2(1, 1);
            mesh.uv = uv;

            return mesh;
        }
    }
}
