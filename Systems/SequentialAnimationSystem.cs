using DOTSHelpers.Systems;
using DOTSSpriteRenderer.BufferElements;
using DOTSSpriteRenderer.Components;
using DOTSSpriteRenderer.Types;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace DOTSSpriteRenderer.Systems {
    [UpdateInGroup(typeof(SpriteRendererSystemGroup))]
    [UpdateBefore(typeof(SpriteRenderSystem))]
    public class SequentialAnimationSystem : JobComponentSystemWithHandles {
        private EntityQuery        _animatorsGroup;
        private SpriteRenderSystem _spriteRenderSystem;

        #region Overrides of ComponentSystemBase
        protected override void OnCreate() {
            base.OnCreate();
            _animatorsGroup = GetEntityQuery(new EntityQueryDesc {
                    All = new[] {
                            ComponentType.ReadWrite<SpriteSequentialAnimator>(),
                            ComponentType.ReadWrite<SpriteCurrentFrame>(),
                    },
            });
            _spriteRenderSystem = World.GetOrCreateSystem<SpriteRenderSystem>();
        }
        #endregion

        #region UpdateSpriteIndexJob
        [BurstCompile]
        private struct UpdateFrameJob : IJobChunk {
            [ReadOnly] public double                            ElapsedTime;
            [ReadOnly] public BufferFromEntity<SpriteFrame>     FrameBufferFromEntity;
            [ReadOnly] public BufferFromEntity<SpriteAnimation> AnimationBufferFromEntity;
            [ReadOnly] public ArchetypeChunkEntityType          EntityType;

            public ArchetypeChunkComponentType<SpriteSequentialAnimator> SequentialAnimatorComponentType;
            public ArchetypeChunkComponentType<SpriteCurrentFrame>       CurrentFrameComponentType;

            #region Implementation of IJobChunk
            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                var animators     = chunk.GetNativeArray(SequentialAnimatorComponentType);
                var currentFrames = chunk.GetNativeArray(CurrentFrameComponentType);
                var entities      = chunk.GetNativeArray(EntityType);

                for (var i = 0; i < chunk.Count; i++) {
                    var animator        = animators[i];
                    var animationHolder = animator.FramesHolder;
                    if (animationHolder == Entity.Null) {
                        animationHolder = entities[i];
                    }

                    var frames     = FrameBufferFromEntity[animationHolder];
                    var animation  = default(SpriteAnimation);
                    var animations = default(DynamicBuffer<SpriteAnimation>);

                    if (animator.StartTime <= 0) {
                        animator.StartTime = ElapsedTime;
                    }

                    if (AnimationBufferFromEntity.Exists(animationHolder)) {
                        animations = AnimationBufferFromEntity[animationHolder];
                        animation = animations[animator.Animation];
                    }
                    else {
                        animation.EndFrame = (ushort)(frames.Length - 1);
                    }

                    var frameIndex      = (int)math.floor((ElapsedTime - animator.StartTime) * animator.FPS);
                    var df              = (animation.StartFrame < animation.EndFrame) ? 1 : -1;
                    var animationLength = df * (animation.EndFrame - animation.StartFrame) + 1;

                    if (frameIndex >= animationLength) {
                        switch (animator.RepeatMode) {
                            case RepeatMode.Once:
                                frameIndex = animationLength - 1;
                                break;
                            case RepeatMode.Loop:
                                frameIndex = frameIndex % animationLength;
                                break;
                            case RepeatMode.NextThenOnce:
                                frameIndex          = 0;
                                animator.StartTime  = ElapsedTime;
                                animator.RepeatMode = RepeatMode.Once;
                                animator.Animation  = animator.NextAnimation;
                                animation           = animations[animator.Animation];
                                break;
                            case RepeatMode.NextThenLoop:
                                frameIndex          = 0;
                                animator.StartTime  = ElapsedTime;
                                animator.RepeatMode = RepeatMode.Loop;
                                animator.Animation  = animator.NextAnimation;
                                animation           = animations[animator.Animation];
                                break;
                            case RepeatMode.NextThenThis:
                                var t = animator.Animation;
                                frameIndex             = 0;
                                animator.Animation     = animator.NextAnimation;
                                animator.NextAnimation = t;
                                animation              = animations[animator.Animation];
                                break;
                        }
                    }

                    var frame = frames[(animation.StartFrame + frameIndex * df)];

                    animators[i]     = animator;
                    currentFrames[i] = (SpriteCurrentFrame)frame;
                }
            }
            #endregion
        }
        #endregion

        #region Overrides of JobComponentSystem
        protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
            inputDeps = new UpdateFrameJob {
                    ElapsedTime                     = Time.ElapsedTime,
                    SequentialAnimatorComponentType = GetArchetypeChunkComponentType<SpriteSequentialAnimator>(),
                    CurrentFrameComponentType       = GetArchetypeChunkComponentType<SpriteCurrentFrame>(),
                    AnimationBufferFromEntity       = GetBufferFromEntity<SpriteAnimation>(),
                    FrameBufferFromEntity           = GetBufferFromEntity<SpriteFrame>(),
                    EntityType                      = GetArchetypeChunkEntityType(),
            }.Schedule(_animatorsGroup, inputDeps);

            _spriteRenderSystem.AddDependency(inputDeps);
            return inputDeps;
        }
        #endregion
    }
}
