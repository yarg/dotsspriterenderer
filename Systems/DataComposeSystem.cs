using DOTSHelpers.Systems;
using DOTSSpriteRenderer.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

namespace DOTSSpriteRenderer.Systems {
    [UpdateInGroup(typeof(SpriteRendererSystemGroup))]
    [UpdateBefore(typeof(SpriteRenderSystem))]
    public class DataComposeSystem : JobComponentSystemWithHandles {
        private EntityQuery        _colorsGroup;
        private SpriteRenderSystem _spriteRenderSystem;

        #region Overrides of ComponentSystemBase
        protected override void OnCreate() {
            base.OnCreate();
            _colorsGroup = GetEntityQuery(new EntityQueryDesc {
                    All = new[] {
                            ComponentType.ReadOnly<SpriteColor>(),
                            ComponentType.ReadWrite<SpriteData>(),
                    },
                    Options = EntityQueryOptions.FilterWriteGroup,
            });
            _spriteRenderSystem = World.GetOrCreateSystem<SpriteRenderSystem>();
        }
        #endregion

        [BurstCompile]
        private struct ComposeColorJob : IJobChunk {
            [ReadOnly] public ArchetypeChunkComponentType<SpriteColor> ColorComponentType;

            public ArchetypeChunkComponentType<SpriteData> DataComponentType;

            #region Implementation of IJobChunk
            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
                var colors = chunk.GetNativeArray(ColorComponentType);
                var datas  = chunk.GetNativeArray(DataComponentType);

                for (var i = 0; i < chunk.Count; i++) {
                    var color = colors[i];

                    var v = datas[i].Value;

                    v.c0.x = color.Add.r;
                    v.c1.x = color.Add.g;
                    v.c2.x = color.Add.b;
                    v.c3.x = color.Add.a;

                    v.c0.y = color.Mul.r;
                    v.c1.y = color.Mul.g;
                    v.c2.y = color.Mul.b;
                    v.c3.y = color.Mul.a;

                    datas[i] = new SpriteData {Value = v};
                }
            }
            #endregion
        }

        #region Overrides of JobComponentSystem
        protected override JobHandle OnUpdateImpl(JobHandle inputDeps) {
            inputDeps = new ComposeColorJob {
                    ColorComponentType = GetArchetypeChunkComponentType<SpriteColor>(true),
                    DataComponentType  = GetArchetypeChunkComponentType<SpriteData>(),
            }.Schedule(_colorsGroup, inputDeps);

            _spriteRenderSystem.AddDependency(inputDeps);
            return inputDeps;
        }
        #endregion
    }
}
