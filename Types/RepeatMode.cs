namespace DOTSSpriteRenderer.Types {
    public enum RepeatMode : byte {
        Once,
        Loop,
        NextThenOnce,
        NextThenLoop,
        NextThenThis,
    }
}
