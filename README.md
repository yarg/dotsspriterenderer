# Sprite renderer for Unity DOTS

Simple sprite renderer for Unity DOTS. Supports:

* Static sprite images
* Frame-based animated sprites
* Rotation-based static images picked from sequence
* Zero memory allocation per frame¹

Native sprite atlases are supported.

## Usage
Use `SpriteUtils` to add sprite to the entity. 
```c#
public class SpriteTest: MonoBehaviour {
    [SerializeField] private Sprite _spriteImage;
    
    private void Satrt() {
        var em = World.DefaultGameObjectInjectionWorld.EntityManager;
        var entity = em.CreateEntity();
        em.AddComponentData(entity, new Translation {
            Value = new float3(0, 0, 10),
        });
        SpriteUtils.AddSprite(em, entity, _spriteImage);
    }
}
```

## Caching
Due to performance reasons its preferable to use `SpriteUtls` method overrides that accepts `CachedSprite` instead of `Sprite`. 
```c#
public class SpriteTest: MonoBehaviour {
    [SerializeField] private Sprite[] _spriteAnimation;

    private void Satrt() {
        var em = World.DefaultGameObjectInjectionWorld.EntityManager;
        var frames = _spriteAnimation.Select(SpriteCache.Cache).ToArray();
        for (var i = 0; i < 100; i++) {
                var entity = em.CreateEntity();
                em.AddComponentData(entity, new Translation {
                    Value = new float3(i, 0, 10),
                });
                SpriteUtils.AddSpriteAnimationToEntity(em, entity, frames);
        }
    }
}
```

## Animations

By default sprite animation will use whole sprite sequence. You can define parts of that sequence using sub-animations and set current sub-animation using `SpriteAnimation.SubAnimationIndex`. To add sub-animations just pass them to `AddSpriteAnimationToEntity` function when creating animated sprite.

Alongside with usual `Once` and `Repeat` animation repeat options you may use `NextThenOnce` and `NextThenThis`. In that cases after current sub-animation finished it will be replaced with `NextSubAnimationIndex` and `RepeatMode` will be changed respectively. For example you may setup idle animation looping after hit animation finished:
```c#
void SetHitAnimation(Entity entity) {
    var animation = EntityManager.GetComponentData<SpriteAnimation>(entity);
    animation.SubAnimationIndex = kHitAnimationIndex;
    animation.NextSubAnimationIndex = kIdelAnimationIndex;
    animation.RepeatMode = RepeatMode.NextThenLoop;
    EntityManager.SetComponentData(entity, animation);
}
```

Last option — `NextThenThis` will swap `SubAnimationIndex` and `NextSubAnimationIndex` when animation finished.

###### Note: its recommended to keep all frames of the animation in the same texture (same texture atlas page) to improve performance.

---
¹ `ManagedComponentStore.InsertSharedComponent<T>(T newData)` allocates 20B per call and cat be fixed this way:
```c#
-    (newDataObj as IRefCounted)?.Retain();
+    if (newDataObj is IRefCounted) {
+        ((IRefCounted)newDataObj).Retain();
+    }
``` 
Probably unity team will fix it sometimes.
