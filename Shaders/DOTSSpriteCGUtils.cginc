void apply_tf(float4x4 tf, float3 in_pos, float2 in_uv, out float4 out_pos, out float2 out_uv) {
    float3   worldPos = tf[0].xyz;
    float    angle    = tf[0].w;
    float3   size     = float3(tf[2].xy, 1);
    float3   offset   = float3(tf[2].zw, 0);
    float3   scale    = float3(tf[3].xy, 1);
    float3   pos      = scale * (size * (in_pos + float3(.5f, .5f, 0)) + offset);
    
    float s;
    float c;
    sincos(angle, s, c); 
    float4x4 mx = float4x4(  c, -s,  0,  0, 
                             s,  c,  0,  0,
                             0,  0,  1,  0,
                             0,  0,  0,  1);
    pos = worldPos + mul(mx, pos);
    
    out_pos = UnityObjectToClipPos(float4(pos, 1.0f));
    out_uv =  lerp(tf[1].xy, tf[1].zw, in_uv);
}
