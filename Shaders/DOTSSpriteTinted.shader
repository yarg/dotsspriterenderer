﻿Shader "DOTSSprite/Tinted" {
    Properties {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _MulColor ("Multiply Color", Color) = (1, 1, 1, 1)
        _AddColor ("Add Color", Color) = (0, 0, 0, 0)
    }
    
    SubShader {
        Tags{
            "Queue"           = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType"      = "Transparent"
        }
        
        Cull Off
        Lighting Off
        ZWrite On
        Blend One OneMinusSrcAlpha
        
        Pass {
            CGPROGRAM
            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight

            #pragma vertex vert
            #pragma fragment frag
            #pragma target 4.5
            #include "DOTSSpriteCGUtils.cginc"

            sampler2D _MainTex;
            half4 _MulColor;
            half4 _AddColor;
            
            StructuredBuffer<float4x4> transformBuffer;
            StructuredBuffer<float4x4> dataBuffer;
            
            struct apptf {
                float3 vertex   : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f {
                float4 pos      : SV_POSITION;
                float2 uv       : TEXCOORD0;
				half4  colorAdd : COLOR0;
				half4  colorMul : COLOR1;
            };
            
            v2f vert (apptf v, uint instanceID : SV_InstanceID) {
                v2f o;
                float4x4 tf = transformBuffer[instanceID];
                apply_tf(tf, v.vertex, v.texcoord, o.pos, o.uv);
                
                float4x4 data = dataBuffer[instanceID];
                o.colorAdd = data[0];
                o.colorMul = data[1]; 
                return o;
            }

            half4 frag (v2f i) : SV_Target{
                half4 col = tex2D(_MainTex, i.uv) * i.colorMul * _MulColor;
				clip(col.a - 1.0 / 255.0);
                col.rgb *= col.a;
				return col + i.colorAdd + _AddColor;
            }
            ENDCG
        }
    }
}
