﻿Shader "DOTSSprite/Default" {
    Properties {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    
    SubShader {
        Tags{
            "Queue"           = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType"      = "Transparent"
        }
        
        Cull Off
        Lighting Off
        ZWrite On
        Blend One OneMinusSrcAlpha
        
        Pass {
            CGPROGRAM
            #pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight

            #pragma vertex vert
            #pragma fragment frag
            #pragma target 4.5
            #include "DOTSSpriteCGUtils.cginc"

            sampler2D _MainTex;
            StructuredBuffer<float4x4> transformBuffer;
            
            struct apptf {
                float3 vertex   : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f {
                float4 pos      : SV_POSITION;
                float2 uv       : TEXCOORD0;
            };
            
            v2f vert (apptf v, uint instanceID : SV_InstanceID) {
                v2f o;
                float4x4 tf = transformBuffer[instanceID];
                apply_tf(tf, v.vertex, v.texcoord, o.pos, o.uv);
                return o;
            }

            half4 frag (v2f i) : SV_Target{
                half4 col = tex2D(_MainTex, i.uv);
				clip(col.a - 1.0 / 255.0);
                col.rgb *= col.a;
				return col;
            }
            ENDCG
        }
    }
}
