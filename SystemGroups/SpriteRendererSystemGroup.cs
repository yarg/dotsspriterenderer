using Unity.Entities;

namespace DOTSSpriteRenderer.Systems {
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public class SpriteRendererSystemGroup : ComponentSystemGroup { }
}
