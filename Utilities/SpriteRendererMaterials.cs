using UnityEngine;

namespace DOTSSpriteRenderer.Utilities {
    public static class SpriteRendererMaterials {
        private static Material _default;
        
        public static Material Default {
            get {
                if (ReferenceEquals(_default, null)) {
                    _default = new Material(Shader.Find("DOTSSprite/Default")) {
                            enableInstancing = true,
                    };
                }

                return _default;
            }
        }

        private static Material _tinted;
        
        public static Material Tinted {
            get {
                if (ReferenceEquals(_tinted, null)) {
                    _tinted = new Material(Shader.Find("DOTSSprite/Tinted")) {
                            enableInstancing = true,
                    };
                }

                return _tinted;
            }
        }
    }
}
