using System;
using System.Linq;
using DOTSSpriteRenderer.BufferElements;
using DOTSSpriteRenderer.Components;
using DOTSSpriteRenderer.Types;
using JetBrains.Annotations;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace DOTSSpriteRenderer.Utilities {
    public static class SpriteRendererUtility {
        /// <summary>
        /// Components required for the entity archetype with simple sprite
        /// </summary>
        public static readonly ComponentType[] SpriteComponents = {
                typeof(SpriteTexture),
                typeof(SpriteCurrentFrame),
        };

        /// <summary>
        /// Components required for the entity archetype with sequentially animated sprite
        /// </summary>
        public static readonly ComponentType[] SequentiallyAnimatedSpriteComponents = SpriteComponents.Concat(new ComponentType[] {
                typeof(SpriteSequentialAnimator),
        }).ToArray();

        /// <summary>
        /// Components required for the entity archetype with rotation-based animated sprite
        /// </summary>
        public static readonly ComponentType[] RotationBasedAnimatedSpriteComponents = SpriteComponents.Concat(new ComponentType[] {
                typeof(Rotation2D),
                typeof(SpriteRotationAnimator),
        }).ToArray();

        /// <summary>
        /// Adds or sets required components to attach given sprite to the entity
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public static void SetupSpriteComponents([NotNull] EntityManager em,
                                                 Entity                  entity,
                                                 [NotNull] Sprite        sprite,
                                                 Material                material = default) {
            #region Checks
            if (em == null) {
                throw new ArgumentNullException(nameof(em));
            }

            if (sprite == null) {
                throw new ArgumentNullException(nameof(sprite));
            }

            if (entity == Entity.Null) {
                throw new ArgumentNullException(nameof(entity));
            }
            #endregion

            if (ReferenceEquals(material, null)) {
                material = SpriteRendererMaterials.Default;
            }

            em.AddSharedComponentData(entity, new SpriteTexture {
                    Texture  = sprite.texture,
                    Material = material,
            });
            AddOrSetComponentData(em, entity, (SpriteCurrentFrame)ExtractSpriteFrame(sprite));
        }

        /// <summary>
        /// Adds or sets required components to attach sequentially animated sprite to the entity
        /// Frames are changed one by one based on animation range, FPS and repeat mode.
        /// 
        /// Animation should declare start and end frame in range of sprites array.
        /// Animation may declare reversed frame order (EndFrame &lt; StartFrame).
        /// If no animations specified - whole sprite sequence will be used
        /// 
        /// All the sprites should use same underlying texture.
        /// Make sure that all sprites are placed on the same page if you use atlases
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static void SetupSequentiallyAnimatedSpriteComponents([NotNull] EntityManager em,
                                                                     Entity                  entity,
                                                                     [NotNull] Sprite[]      sprites,
                                                                     int                     fps                = 60, //0..255
                                                                     RepeatMode              repeatMode         = RepeatMode.Loop,
                                                                     int                     animationIndex     = 0,
                                                                     int                     nextAnimationIndex = 0,
                                                                     SpriteAnimation[]       animations         = default,
                                                                     Material                material           = default) {
            #region Checks
            if (em == null) {
                throw new ArgumentNullException(nameof(em));
            }

            if (entity == Entity.Null) {
                throw new ArgumentNullException(nameof(entity));
            }

            if ((sprites == null) || (sprites.Length == 0)) {
                throw new ArgumentNullException(nameof(sprites));
            }

            if (fps < 0) {
                throw new ArgumentOutOfRangeException(nameof(fps), "cannot be negative");
            }

            if ((animations != null) && (animations.Length > 0)) {
                if ((animationIndex < 0) || (animationIndex >= animations.Length)) {
                    throw new ArgumentOutOfRangeException(nameof(animationIndex), "outside of animations array bounds");
                }

                if ((nextAnimationIndex < 0) || (nextAnimationIndex >= animations.Length)) {
                    throw new ArgumentOutOfRangeException(nameof(nextAnimationIndex), "outside of animations array bounds");
                }
            }
            #endregion

            SetupSpriteFrameComponents(em, entity, sprites, material);
            AddOrSetComponentData(em, entity, new SpriteSequentialAnimator {
                    Animation     = (ushort)animationIndex,
                    NextAnimation = (ushort)nextAnimationIndex,
                    RepeatMode    = repeatMode,
                    FPS           = (byte)fps,
                    FramesHolder  = entity,
            });

            if ((animations != null) && (animations.Length > 0)) {
                var anims = AddOrGetBuffer<SpriteAnimation>(em, entity);
                anims.Capacity = animations.Length;
                foreach (var a in animations) {
                    anims.Add(a);
                    //TODO: check bounds
                }
            }
        }

        public static void ChangeAnimation([NotNull] EntityManager em,
                                           Entity                  entity,
                                           RepeatMode              repeatMode         = RepeatMode.Loop,
                                           int                     animationIndex     = 0,
                                           int                     nextAnimationIndex = 0) {
            var anim = em.GetComponentData<SpriteSequentialAnimator>(entity);
            anim.Animation     = (ushort)animationIndex;
            anim.NextAnimation = (ushort)nextAnimationIndex;
            anim.RepeatMode    = repeatMode;
            anim.StartTime     = 0;
            em.SetComponentData(entity, anim);
        }

        /// <summary>
        /// Adds or sets required components to attach sequentially animated sprite to the entity
        /// Frames are changed one by one based on animation range, FPS and repeat mode.
        /// 
        /// Animation should declare start and end frame in range of sprites array.
        /// Animation may declare reversed frame order (EndFrame &lt; StartFrame).
        /// If no animations specified - whole sprite sequence will be used
        /// 
        /// All the sprites should use same underlying texture.
        /// Make sure that all sprites are placed on the same page if you use atlases
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public static void SetupSequentiallyAnimatedSpriteComponents([NotNull] EntityManager  em,
                                                                     Entity                   entity,
                                                                     [NotNull] SpriteFrame[]  spriteFrames,
                                                                     [NotNull] Texture        texture,
                                                                     int                      fps                = 60, //0..255
                                                                     RepeatMode               repeatMode         = RepeatMode.Loop,
                                                                     int                      animationIndex     = 0,
                                                                     int                      nextAnimationIndex = 0,
                                                                     params SpriteAnimation[] animations) {
            #region Checks
            if (em == null) {
                throw new ArgumentNullException(nameof(em));
            }

            if (entity == Entity.Null) {
                throw new ArgumentNullException(nameof(entity));
            }

            if ((spriteFrames == null) || (spriteFrames.Length == 0)) {
                throw new ArgumentNullException(nameof(spriteFrames));
            }

            if (fps < 0) {
                throw new ArgumentOutOfRangeException(nameof(fps), "cannot be negative");
            }

            if (animations.Length > 0) {
                if ((animationIndex < 0) || (animationIndex >= animations.Length)) {
                    throw new ArgumentOutOfRangeException(nameof(animationIndex), "outside of animations array bounds");
                }

                if ((nextAnimationIndex < 0) || (nextAnimationIndex >= animations.Length)) {
                    throw new ArgumentOutOfRangeException(nameof(nextAnimationIndex), "outside of animations array bounds");
                }
            }

            if (texture == null) {
                throw new ArgumentNullException(nameof(texture));
            }
            #endregion

            SetupSpriteFrameComponents(em, entity, spriteFrames, texture);
            AddOrSetComponentData(em, entity, new SpriteSequentialAnimator {
                    Animation     = (ushort)animationIndex,
                    NextAnimation = (ushort)nextAnimationIndex,
                    RepeatMode    = repeatMode,
                    FPS           = (byte)fps,
                    FramesHolder  = entity,
            });

            if (animations.Length > 0) {
                var anims = AddOrGetBuffer<SpriteAnimation>(em, entity);
                anims.Capacity = animations.Length;
                foreach (var a in animations) {
                    anims.Add(a);
                    //TODO: check bounds
                }
            }
        }

        /// <summary>
        /// Adds or sets required components to attach frame-based animated sprite to the entity
        /// Frame is shown based on Z angle of entity <see cref="Rotation2D"/> component data.
        /// Sprite frames should start with 0 degree (right direction) and follow counter-clockwise prder.
        /// Its recommended to keep frames number multiple of 4.
        ///
        /// All the sprites should use same underlying texture.
        /// Make sure that all sprites are placed on the same page if you use atlases
        /// </summary>
        public static void SetupRotationBasedAnimatedSpriteComponents([NotNull] EntityManager em,
                                                                      Entity                  entity,
                                                                      [NotNull] Sprite[]      sprites) {
            #region Checks
            if (em == null) {
                throw new ArgumentNullException(nameof(em));
            }

            if (entity == Entity.Null) {
                throw new ArgumentNullException(nameof(entity));
            }

            if ((sprites == null) || (sprites.Length == 0)) {
                throw new ArgumentNullException(nameof(sprites));
            }
            #endregion

            SetupSpriteFrameComponents(em, entity, sprites);
            AddOrSetComponentData(em, entity, new SpriteRotationAnimator {
                    FramesHolder = entity,
            });
            AddOrSetComponentData(em, entity, new Rotation2D());
        }

        /// <summary>
        /// Adds or sets required components to attach frame-based animated sprite to the entity
        /// Frame is shown based on Z angle of entity <see cref="Rotation2D"/> component data.
        /// Sprite frames should start with 0 degree (right direction) and follow counter-clockwise prder.
        /// Its recommended to keep frames number multiple of 4.
        ///
        /// All the sprites should use same underlying texture.
        /// Make sure that all sprites are placed on the same page if you use atlases
        /// </summary>
        public static void SetupRotationBasedAnimatedSpriteComponents([NotNull] EntityManager em,
                                                                      Entity                  entity,
                                                                      [NotNull] SpriteFrame[] spriteFrames,
                                                                      [NotNull] Texture       texture) {
            #region Checks
            if (em == null) {
                throw new ArgumentNullException(nameof(em));
            }

            if (entity == Entity.Null) {
                throw new ArgumentNullException(nameof(entity));
            }

            if ((spriteFrames == null) || (spriteFrames.Length == 0)) {
                throw new ArgumentNullException(nameof(spriteFrames));
            }

            if (texture == null) {
                throw new ArgumentNullException(nameof(texture));
            }
            #endregion

            SetupSpriteFrameComponents(em, entity, spriteFrames, texture);
            AddOrSetComponentData(em, entity, new SpriteRotationAnimator {
                    FramesHolder = entity,
            });

            AddOrSetComponentData(em, entity, new Rotation2D());
        }

        /// <summary>
        /// Adds or sets sprite frames, texture and current frame components for custom frame-based sprite animation
        ///
        /// All the sprites should use same underlying texture.
        /// Make sure that all sprites are placed on the same page if you use atlases
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static void SetupSpriteFrameComponents([NotNull] EntityManager em, Entity entity, [NotNull] Sprite[] sprites, Material material = default) {
            #region Checks
            if (em == null) {
                throw new ArgumentNullException(nameof(em));
            }

            if (entity == Entity.Null) {
                throw new ArgumentNullException(nameof(entity));
            }

            if ((sprites == null) || (sprites.Length == 0)) {
                throw new ArgumentNullException(nameof(sprites));
            }
            #endregion

            Texture texture = default;
            var     frames  = AddOrGetBuffer<SpriteFrame>(em, entity);
            frames.Capacity = sprites.Length;

            for (var index = 0; index < sprites.Length; index++) {
                var sprite = sprites[index];
                if (texture == null) {
                    texture = sprite.texture;
                }

                if (texture != sprite.texture) {
                    //TODO: more info
                    throw new ArgumentException($"All the sprites should use same underlying texture (same atlas page). Sprite #${index} uses '${sprite.texture}' while  others use '${texture}'.", nameof(sprites));
                }

                frames.Add(ExtractSpriteFrame(sprite));
            }

            if (ReferenceEquals(material, null)) {
                material = SpriteRendererMaterials.Default;
            }

            em.AddSharedComponentData(entity, new SpriteTexture {
                    Texture  = texture,
                    Material = material,
            });

            AddOrSetComponentData(em, entity, new SpriteCurrentFrame());
        }

        /// <summary>
        /// Adds or sets sprite frames, texture and current frame components for custom frame-based sprite animation
        ///
        /// All the sprites should use same underlying texture.
        /// Make sure that all sprites are placed on the same page if you use atlases
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static void SetupSpriteFrameComponents([NotNull] EntityManager em,
                                                      Entity                  entity,
                                                      [NotNull] SpriteFrame[] spriteFrames,
                                                      [NotNull] Texture       texture,
                                                      Material                material = default) {
            #region Checks
            if (em == null) {
                throw new ArgumentNullException(nameof(em));
            }

            if (entity == Entity.Null) {
                throw new ArgumentNullException(nameof(entity));
            }

            if ((spriteFrames == null) || (spriteFrames.Length == 0)) {
                throw new ArgumentNullException(nameof(spriteFrames));
            }

            if (texture == null) {
                throw new ArgumentNullException(nameof(texture));
            }
            #endregion

            var frames = AddOrGetBuffer<SpriteFrame>(em, entity);
            frames.Capacity = spriteFrames.Length;
            for (var index = 0; index < spriteFrames.Length; index++) {
                frames.Add(spriteFrames[index]);
            }

            if (ReferenceEquals(material, null)) {
                material = SpriteRendererMaterials.Default;
            }

            em.AddSharedComponentData(entity, new SpriteTexture {
                    Texture  = texture,
                    Material = material,
            });

            AddOrSetComponentData(em, entity, new SpriteCurrentFrame());
        }

        /// <summary>
        /// Converts clockwise frame sequence started with XII hours to math-friendly counter-clockwise sequence accepted with <see cref="SetupRotationBasedAnimatedSpriteComponents"/>
        /// </summary>
        /// <param name="clockwiseSequence"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentException"></exception>
        public static Sprite[] SequenceTo360([NotNull] Sprite[] clockwiseSequence) {
            if (clockwiseSequence == null) {
                throw new ArgumentNullException(nameof(clockwiseSequence));
            }

            if ((clockwiseSequence.Length % 4) != 0) {
                throw new ArgumentException("Length should be multiplier of 4", nameof(clockwiseSequence));
            }

            var startFrame = clockwiseSequence.Length / 4;
            var sequence   = new Sprite[clockwiseSequence.Length];
            var index      = 0;

            for (var i = startFrame; i >= 0; i--) {
                sequence[index++] = clockwiseSequence[i];
            }

            for (var i = clockwiseSequence.Length - 1; i > startFrame; i--) {
                sequence[index++] = clockwiseSequence[i];
            }

            return sequence;
        }

        /// <summary>
        /// Extracts sprite info to the SpriteFrame struct
        /// </summary>
        /// <returns></returns>
        public static SpriteFrame ExtractSpriteFrame(Sprite sprite) {
            var rect = sprite.textureRect;
            var texSize = new float4(sprite.texture.width, sprite.texture.height,
                                     sprite.texture.width, sprite.texture.height);
            return new SpriteFrame {
                    UV     = new float4(rect.xMin, rect.yMin, rect.xMax, rect.yMax) / texSize,
                    Size   = new float2(rect.width, rect.height)                    / sprite.pixelsPerUnit,
                    Offset = -(sprite.pivot - sprite.textureRectOffset)             / sprite.pixelsPerUnit,
            };
        }

        private static void AddOrSetComponentData<T>(EntityManager em, Entity entity, T componentData)
                where T : struct, IComponentData {
            if (em.HasComponent<T>(entity)) {
                em.SetComponentData(entity, componentData);
            }
            else {
                em.AddComponentData(entity, componentData);
            }
        }

        private static DynamicBuffer<T> AddOrGetBuffer<T>(EntityManager em, Entity entity)
                where T : struct, IBufferElementData {
            if (em.HasComponent<T>(entity)) {
                return em.GetBuffer<T>(entity);
            }

            return em.AddBuffer<T>(entity);
        }
    }
}
