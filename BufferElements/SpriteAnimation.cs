using Unity.Entities;

namespace DOTSSpriteRenderer.BufferElements {
    [InternalBufferCapacity(1)]
    public struct SpriteAnimation : IBufferElementData {
        public ushort StartFrame;
        public ushort EndFrame;

        public SpriteAnimation(ushort startFrame, ushort endFrame) {
            StartFrame = startFrame;
            EndFrame   = endFrame;
        }
    }
}
