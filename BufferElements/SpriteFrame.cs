using Unity.Entities;
using Unity.Mathematics;

namespace DOTSSpriteRenderer.BufferElements {
    [InternalBufferCapacity(1)]
    public struct SpriteFrame : IBufferElementData {
        public float4 UV;
        public float2 Size;
        public float2 Offset;
    }
}
